from beem import Hive
from beem.account import Account

hive = Hive()

# Set the delegation range and target value as constants
MIN_HP_DELEGATION = 95
MAX_HP_DELEGATION = 105
HP_TARGET_VALUE = 50

# Prompt the user for their Hive account name and active key
account_name = input("Enter your Hive account name: ")
active_key = input("Enter your Hive active key: ")

# Connect to the Hive blockchain using the user's information
account = Account(account_name, keys=[active_key], hive_instance=hive)


def check_and_update_delegation():
    # Get the delegate vests
    dvests = account.get_vesting_delegations()

    # Check the delegation range and get the delegatees
    # within the range
    delegatees_in_range = []
    for dvest in dvests:
        amount = float(
            dvest['vesting_shares']['amount']
        ) / 10**dvest['vesting_shares']['precision']
        hp = hive.vests_to_hp(amount)
        if MIN_HP_DELEGATION <= hp <= MAX_HP_DELEGATION:
            delegatees_in_range.append(dvest['delegatee'])

    # Update the delegation for each delegatee within the range
    vests = hive.hp_to_vests(HP_TARGET_VALUE)
    for delegatee in delegatees_in_range:
        delegator = Account(
            account_name,
            keys=[active_key],
            hive_instance=hive
        )
        delegatee_account = Account(
            delegatee,
            hive_instance=hive
        )
        delegator.delegate_vesting_shares(
            delegatee_account,
            vests,
            account=delegator
        )

    # Get the updated delegate vests
    updated_dvests = account.get_vesting_delegations()
    return updated_dvests


# Check the delegation range and update the delegation if necessary
updated_dvests = check_and_update_delegation()

# Print the updated delegate vests
for dvest in updated_dvests:
    amount = float(
        dvest['vesting_shares']['amount']
        ) / 10**dvest['vesting_shares']['precision']
    hp = hive.vests_to_hp(amount)
    print(f"{dvest['delegatee']}: {hp:.3f} HP")
