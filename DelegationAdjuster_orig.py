from beem import Hive
from beem.account import Account
# from beem.amount import Amount

hive = Hive()


def get_user_info():
    account_name = input("Enter your Hive account name: ")
    active_key = input("Enter your Hive active key: ")
    return account_name, active_key


# Connect to the Hive blockchain using the user's information
def connect_to_hive(account_name, active_key):
    # Connect to the Hive blockchain using the user's account
    # name and active key.
    account = Account(account_name, hive_instance=hive)
    return account


# Get the range of Hive Power within the delegation changes
# are going to be made and the target value of Hive Power.
def get_delegation_range():
    min_hp_delegation = float(input(
        "Enter the minimum Hive Power within the range: "
    ))
    max_hp_delegation = float(input(
        "Enter the maximum Hive Power within the range: "
    ))
    hp_target_value = float(input(
        "Enter the Hive Power value you want to change to: "
    ))
    return min_hp_delegation, max_hp_delegation, hp_target_value


def convert_to_hp(dvests):
    hp_list = []
    for dvest in dvests:
        amount = float(
            dvest['vesting_shares']['amount']
        ) / 10**dvest['vesting_shares']['precision']
        hp = hive.vests_to_hp(amount)
        hp_list.append((dvest['delegatee'], hp))
        print(f"Delegated to {dvest['delegatee']}: {hp:.3f} HP")
    return hp_list


def change_delegation(delegatees_in_range, hp_target_value, account_name):
    vests = hive.hp_to_vests(hp_target_value)
    for delegatee in delegatees_in_range:
        delegator = Account(account_name, hive_instance=hive)
        delegatee_account = Account(delegatee[0], hive_instance=hive)
        delegator.delegate_vesting_shares(
            delegatee_account,
            vests,
            account=delegator
        )


def get_and_show_updated_delegatees(delegatees_in_range)
    updated_dvests = account.get_vesting_delegations()
    updated_hp_list = convert_to_hp(updated_dvests)
    for updated_delegatee, updated_hp in updated_hp_list:
        if updated_delegatee == delegatee[0]:
            print(f"After change: {updated_delegatee} - {updated_hp} HP")
            break


def check_delegation_range(hp_list, min_hp_delegation, max_hp_delegation):
    delegatees_in_range = []
    for delegatee, hp in hp_list:
        if min_hp_delegation <= hp <= max_hp_delegation:
            delegatees_in_range.append((delegatee, hp))
    return delegatees_in_range


def main():
    # Call get_user_info function to get user information.
    account_name, active_key = get_user_info()

    # Get the delegation range and target value to change
    # the delegations in the range to.
    min_hp_delegation, max_hp_delegation, hp_target_value = (
        get_delegation_range()
    )

    # Connect to the Hive blockchain useing the user's information
    account = connect_to_hive(account_name, active_key)

    # Get the delegate vesting shares (dvests)
    dvests = account.get_vesting_delegations()

    # Call the 'convert_to_hp' function and pass 'dvests' as an argument
    hp_list = convert_to_hp(dvests)

    delegatees_in_range = check_delegation_range(
        hp_list, min_hp_delegation, max_hp_delegation
    )

    # Update the delegation for each delegatee within the range
    change_delegation(delegatees_in_range, hp_target_value, account_name)


if __name__ == "__main__":
    main()
